<?php

namespace Database\Factories;

use App\Entities\Books\Book;
use Illuminate\Database\Eloquent\Factories\Factory;


class BooksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(),
            'isbn_number'=> $this->faker->numerify('###############'),
            'published_year'=>$this->faker->numerify('####'),

        ];
    }
}
