<?php

namespace Tests\Feature\AutoGen\API\V1;

use App\Entities\Books\Book;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksGetBooksAPITest extends APIBaseTestCase
{
    use DatabaseTransactions;

    /**
     *
     *
     *
     * @return  void
     */
    public function test_api_books_get_get_books()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;
        Book::truncate();
        $user=User::factory()->create();
        $user->books()->create([
            'name'=>'name',
            'isbn_number'=>'12345',
            'published_year'=>'2005'
        ]);
        
        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();
                                            
        
        $response = $this->get('/api/v1/books', $headers);
                
        $this->saveResponse($response->getContent(), 'books_get_get_books', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [[
            'name' => 'name', 'isbn_number' => '12345', 'published_year' => '2005', 'author_id' => $user->id
        ]]]);
    }
    public function test_api_books_get_get_paginator()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;
        Book::truncate();
        $user = User::factory()->create();
        $user->books()->create([
            'name' => 'name',
            'isbn_number' => '12345',
            'published_year' => '2005'
        ]);
        Book::factory()->count(50)->create([
            'author_id'=>$user->id
        ]);

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?page=1&per_page=10', $headers);

        $this->saveResponse($response->getContent(), 'books_get_get_books', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJsonCount(10, 'payload');
        $response->assertJson(['paginator' => ['current_page' => 1]]);
    }

    public function test_api_books_get_get_search()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;
        Book::truncate();
        $user = User::factory()->create();
        $user->books()->create([
            'name' => 'name',
            'isbn_number' => '12345',
            'published_year' => '2005'
        ]);
        Book::factory()->count(50)->create([
            'author_id'=>$user->id
        ]);

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?q=name', $headers);

        $this->saveResponse($response->getContent(), 'books_get_get_books', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [[
            'name' => 'name', 'isbn_number' => '12345', 'published_year' => '2005', 'author_id' => $user->id
        ]]]);

    }
    public function test_api_books_get_data_type()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;
        Book::truncate();
        $user = User::factory()->create();
        $book=$user->books()->create([
            'name' => 'name',
            'isbn_number' => '12345',
            'published_year' => '2005'
        ]);
        

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?q=name', $headers);

        $this->saveResponse($response->getContent(), 'books_get_get_books', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [[
            'id' => (string) $book->id,
            'name' => (string) $book->name,
            'isbn_number' => (string) $book->isbn_number,
            'published_year' =>(string)$book->published_year
        ]]], true);
    }
    public function test_api_books_get_get_sort_asc_search()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;
        Book::truncate();
        $user = User::factory()->create();
        $user->books()->create([
            'name' => 'name',
            'isbn_number' => '12345',
            'published_year' => '2005'
        ]);
        Book::factory()->count(50)->create([
            'author_id' => $user->id
        ]);

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?q=name&sort=asc', $headers);

        $this->saveResponse($response->getContent(), 'books_get_get_books', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [[
            'name' => 'name', 'isbn_number' => '12345', 'published_year' => '2005', 'author_id' => $user->id
        ]]]);
    }
    public function test_api_books_get_get_sort_desc_search()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;
        Book::truncate();
        $user = User::factory()->create();
        $user->books()->create([
            'name' => 'name',
            'isbn_number' => '12345',
            'published_year' => '2005'
        ]);
        Book::factory()->count(50)->create([
            'author_id' => $user->id
        ]);

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books?q=name&sort=desc', $headers);

        $this->saveResponse($response->getContent(), 'books_get_get_books', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [[
            'name' => 'name', 'isbn_number' => '12345', 'published_year' => '2005', 'author_id' => $user->id
        ]]]);
    }
}
