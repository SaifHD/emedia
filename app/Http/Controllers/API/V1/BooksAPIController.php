<?php

namespace App\Http\Controllers\API\V1;

use App\Entities\Books\Book;
use App\Entities\Books\BooksRepository;
use App\Http\Controllers\API\V1\APIBaseController;
use EMedia\Api\Docs\APICall;
use Illuminate\Http\Request;

class BooksAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(BooksRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
                	return (new APICall())
                        ->setGroup('Books')
                        ->setName('Get Books')
                	    ->setParams([
                	        'q|Search query',
                	        'page|Page number',
                            'per_page| Number Of Books Per Page',
                            'sort | Sort Using Published Date'
                        ])
                        ->setSuccessPaginatedObject(Book::class);
                });
        if($request->query('sort')=="asc"){
            $items=Book::orderBy('published_year', $request->query('sort'))
                ->where('name','LIKE','%'.$request->query('q').'%')->with('author');
            return response()->apiSuccessPaginated($items->paginate($request->query('per_page',20)));
        }
        elseif($request->query('sort') == "desc") {
            $items = Book::orderBy('published_year', $request->query('sort'))
                ->where('name', 'LIKE', '%' . $request->query('q') . '%')->with('author');
            return response()->apiSuccessPaginated($items->paginate($request->query('per_page', 20)));
        }
        
        $filter = $this->repo->newSearchFilter();
        $filter->setPerPage($request->query('per_page', 20));

        $items = $this->repo->search($filter);

		return response()->apiSuccessPaginated($items);
	}

}
