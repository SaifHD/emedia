<?php

namespace App\Entities\Books;

use App\Models\User;
use EMedia\Formation\Entities\GeneratesFields;
use ElegantMedia\SimpleRepository\Search\Eloquent\SearchableLike;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Database\Factories\BooksFactory;

class Book extends Model
{

    use HasFactory;
    use SearchableLike;
    use GeneratesFields;

    // use \Cviebrock\EloquentSluggable\Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    /*
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    */

    protected $fillable = [
        'name', 'author_id', 'isbn_number', 'published_year'
    ];

    protected $searchable = [
        'name'
    ];

    protected $editable = [
        'name',
    ];
    protected $casts = [
        'id' => 'string'
    ];
    protected static function newFactory()
    {
        return BooksFactory::new();
    }
    /**
     *
     * Add any update only validation rules for this model
     *
     * @return array
     */
    public function getCreateRules()
    {
        return [
            'name' => 'required',
        ];
    }

    /**
     *
     * Add any update only validation rules for this model
     *
     * @return array
     */
    public function getUpdateRules()
    {
        return [
            'name' => 'required',
        ];
    }

    /**
     *
     * Add any update only validation messations
     *
     * @return array
     */
    public function getCreateValidationMessages()
    {
        return [];
    }

    /**
     *
     * Add any update only validation messations
     *
     * @return array
     */
    public function getUpdateValidationMessages()
    {
        return [];
    }
    public function author(){
        return $this->belongsTo(User::class);
    }
}
