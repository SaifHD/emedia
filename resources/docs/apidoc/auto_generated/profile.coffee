# ******************************************************** #
#           AUTO-GENERATED. DO NOT EDIT THIS FILE.         #
# ******************************************************** #
#    Create your files in `resources/docs/apidoc/manual`   #
# ******************************************************** #
###
@apiDescription Get currently logged in user's profile
@apiVersion 1.0.0
@api {GET} api/v1/profile My Profile
@apiGroup Profile
@apiUse default_headers
@apiSuccessExample {json} Success-Response / HTTP 200 OK
{
    "payload": {
        "uuid": "e68e0e0b-0f6b-41a2-8ca2-c5f588baa755",
        "last_name": null,
        "email": "apps+suadmin@elegantmedia.com.au",
        "avatar_url": null,
        "timezone": "Australia\/Melbourne",
        "first_name": "Tony Stark (SUPER-ADMIN)",
        "full_name": "Tony Stark (SUPER-ADMIN)"
    },
    "message": "",
    "result": true
}
###
# ******************************************************** #
#           AUTO-GENERATED. DO NOT EDIT THIS FILE.         #
# ******************************************************** #
#    Create your files in `resources/docs/apidoc/manual`   #
# ******************************************************** #
###
@apiVersion 1.0.0
@api {PUT} api/v1/profile Update My Profile
@apiGroup Profile
@apiParam {String} first_name First name
@apiParam {String} [last_name] Last name
@apiParam {String} email Email
@apiParam {String} [phone] Phone
@apiUse default_headers
@apiSuccessExample {json} Success-Response / HTTP 200 OK
{
    "payload": true,
    "message": "",
    "result": true
}
###
# ******************************************************** #
#           AUTO-GENERATED. DO NOT EDIT THIS FILE.         #
# ******************************************************** #
#    Create your files in `resources/docs/apidoc/manual`   #
# ******************************************************** #
###
@apiVersion 1.0.0
@api {POST} api/v1/avatar Update My Avatar
@apiGroup Profile
@apiParam {File} image Image
@apiUse default_headers
@apiSuccessExample {json} Success-Response / HTTP 200 OK
{
    "payload": {
        "uuid": "e68e0e0b-0f6b-41a2-8ca2-c5f588baa755",
        "last_name": null,
        "email": "apps+suadmin@elegantmedia.com.au",
        "avatar_url": "http:\/\/localhost:8000\/storage\/avatars\/4\/sN9IMzuJtI3KLQc9lOA7C9thAPk5KlHV2bORvxbZ.jpg",
        "timezone": "Australia\/Melbourne",
        "first_name": "Tony Stark (SUPER-ADMIN)",
        "full_name": "Tony Stark (SUPER-ADMIN)"
    },
    "message": "",
    "result": true
}
###
